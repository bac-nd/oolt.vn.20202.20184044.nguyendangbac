package GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import order.Order;
import media.*;

import GUI.CHILD_WINDOW.*;

import java.awt.event.*;
import java.awt.*;

public class Window extends JFrame implements ActionListener {
	private String caption;
	private JButton addBook;
	private JButton addDVD;
	private JButton addCD;
	private JButton deleteTtem;
	private JButton close;
	private JTable table;
	private DefaultTableModel model;

	private Order order;
	
	public Window(Order order) {
		caption = "THE ITEMS LIST OF ORDER";
		addBook = new JButton("Add book");
		addDVD = new JButton("Add DVD");
		addCD = new JButton("Add CD");
		deleteTtem = new JButton("Delete selected item");
		// close = new JButton("Close");

		addBook.addActionListener(this);
		addDVD.addActionListener(this);
		addCD.addActionListener(this);
		deleteTtem.addActionListener(this);
		// close.addActionListener(this);

		table = new JTable();


		this.order = order;
		init();

	}

	public void init() {
		// setSize(800, 600);

		JPanel window = new JPanel();
		window.setBackground(Color.LIGHT_GRAY);
		window.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		window.setLayout(new BoxLayout(window, BoxLayout.Y_AXIS));

		JPanel upWindow = new JPanel();
		upWindow.setBorder(BorderFactory.createTitledBorder(caption));
		upWindow.setBackground(Color.WHITE);
		JScrollPane list = new JScrollPane(table);
		model = new DefaultTableModel() {
			public Class<?> getColumnClass(int col) {
				switch(col) {
					case 2: return Boolean.class;
					default: return String.class;
				}
			}
			public boolean isCellEditable(int row, int col) {
				if (col == 2) return true;
				return false;
			}
		};
		model.addColumn("STT");
		model.addColumn("Info");
		model.addColumn("Select");
		update();
		table.setModel(model);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		table.getColumnModel().getColumn(2).setMaxWidth(50);
		// table.setModel(arg0);
		upWindow.add(list);

		JPanel downWindow = new JPanel();
		downWindow.setLayout(new BoxLayout(downWindow, BoxLayout.X_AXIS));
		downWindow.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		downWindow.add(addBook);
		downWindow.add(addDVD);
		downWindow.add(addCD);
		downWindow.add(deleteTtem);
		// downWindow.add(close);
		
		window.add(upWindow);
		window.add(downWindow);
		add(window);
		pack();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Aims Project");

		setResizable(false);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		// System.out.println(e.getActionCommand());
		switch(e.getActionCommand()) {
			case "Add book": {
				new BookAddForm(this, "Add Book");
				break;
			}
			case "Add DVD": {
				new DVDAddForm(this, "Add DVD");
				break;
			}
			case "Add CD": {
				new CDAddForm(this, "Add CD");
			}
			case "Delete selected item": {
				break;
			}
			case "Close": {
				dispose();
				break;
			}
		}
		// System.out.println(e.getActionCommand());
	}

	public void update() {
		model.setRowCount(0);
		for (int i = 0; i < order.getItemsOrdered().size(); i++) {
			model.addRow(new Object[0]);
			model.setValueAt(i+1, i, 0);
			model.setValueAt(order.getItemsOrdered().get(i).outString(), i, 1);
			model.setValueAt(false, i, 2);
		}
	}

	public Order getOrder() {
		return order;
	}
}
