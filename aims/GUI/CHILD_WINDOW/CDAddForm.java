package GUI.CHILD_WINDOW;

import GUI.Window;
import media.CompactDisc;

public class CDAddForm extends DVDAddForm {

    public CDAddForm(Window window, String name) {
        super(window, name);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void addItem() {
        // TODO Auto-generated method stub
        String title = this.gTitle().getText();
        String category = this.gCategory().getText();
        String cost = this.gCost().getText();
        String director = this.gDirector().getText();
        String length = this.gLength().getText();
        if (title == "") {
            new Error();
            return;
        }
        if (category == "") {
            this.gWindow().getOrder().addMedia(new CompactDisc(title));
            this.gWindow().update();
            return;
        }
        if (cost == "") {
            this.gWindow().getOrder().addMedia(new CompactDisc(title, category));
            this.gWindow().update();
            return;
        }
        for (int i = 0; i < cost.length(); i++) {
            if (cost.charAt(i) < '0' || cost.charAt(i) > '9' || cost.charAt(i) != '.') {
                new Error();
                return;
            }
        }
        if (length == "") {
            this.gWindow().getOrder().addMedia(
                new CompactDisc(title, category, Float.parseFloat(cost))
            );
            this.gWindow().update();
            return;
        }
        for (int i = 0; i < length.length(); i++) {
            if (length.charAt(i) < '0' || length.charAt(i) > '9') {
                new Error();
                return;
            }
        }
        if (director == "") {
            this.gWindow().getOrder().addMedia(
                new CompactDisc(
                    title, category, 
                    Float.parseFloat(cost), 
                    Integer.parseInt(length)
                )
            );
            this.gWindow().update();
            return;
        }
        this.gWindow().getOrder().addMedia(
            new CompactDisc(
                title, category, 
                Float.parseFloat(cost), 
                Integer.parseInt(length),
                director
            )
        );
        this.gWindow().update();
        return;
    }

}
