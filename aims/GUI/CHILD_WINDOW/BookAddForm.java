package GUI.CHILD_WINDOW;

import javax.swing.*;

import GUI.Window;
import media.Book;
import order.Order;

public class BookAddForm extends Chill{
    private JTextArea authorBox;
    
    private Window window;

    public BookAddForm(Window window, String name) {
        super(name);
        this.window = window;
    }
    
    @Override
    public JPanel specialAdd() {
        // TODO Auto-generated method stub
        JPanel result = new JPanel();
        // result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));

        authorBox = new JTextArea(5, 38);
        authorBox.setLineWrap(true);
        authorBox.setWrapStyleWord(true);

        // result.setLayout(new BoxLayout(result, BoxLayout.PAGE_AXIS));
        result.setBorder(BorderFactory.createTitledBorder("Authors (persionA, persionB, ...):"));
        result.add(authorBox);
        return result;
    }

    @Override
    public void addItem() {
        // TODO Auto-generated method stub
        String title = this.gTitle().getText();
        String category = this.gCategory().getText();
        String cost = this.gCost().getText();
        String authors = this.gAuthorBox().getText();
        if (title == "") {
            new Error();
            return;
        }
        if (category != "") {
            if (cost == "" || authors == "") {
                window.getOrder().addMedia(new Book(title, category));
                window.update();
                return;
            }
            for (int i = 0; i < cost.length(); i++) {
                if (cost.charAt(i) < '0' || cost.charAt(i) > '9') {
                    new Error();
                    return;
                }
            }
            String [] Authors = authors.split(",");
            window.getOrder().addMedia(new Book(title, category, Float.parseFloat(cost), Authors));
            window.update();
            return;
        }
        window.getOrder().addMedia(new Book(title));
        window.update();
    }

    public JTextArea gAuthorBox() {
        return authorBox;
    }

    public void sAuthorBox(JTextArea authorBox) {
        this.authorBox = authorBox;
    }

}
