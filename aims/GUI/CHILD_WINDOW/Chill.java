package GUI.CHILD_WINDOW;

import javax.swing.*;

import order.Order;

import java.awt.*;
import java.awt.event.*;

public abstract class Chill extends JFrame implements ActionListener{
	private JButton add;
	private JButton close;

	private String name;

	private JTextField title;

	private JTextField category;
	private JTextField cost;

    public Chill(String name) {
		this.name = name;

		add = new JButton("Add");
		close = new JButton("Close");
		this.title = new JTextField(38);
		this.category = new JTextField(38);
		this.cost = new JTextField(38);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBackground(Color.LIGHT_GRAY);

		init();
    }

	public void init() {
		JPanel window = new JPanel();
		window.setLayout(new BoxLayout(window, BoxLayout.Y_AXIS));
		window.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		JPanel title = new JPanel();
		title.setBorder(BorderFactory.createTitledBorder("Title:"));
		title.add(this.title);

		JPanel category = new JPanel();
		category.setBorder(BorderFactory.createTitledBorder("Category:"));
		category.add(this.category);
		
		JPanel cost = new JPanel();
		cost.setBorder(BorderFactory.createTitledBorder("Cost:"));
		cost.add(this.cost);

        JPanel tail = specialAdd();

		JPanel buttonGroup = new JPanel();
		add.setAlignmentX(Component.LEFT_ALIGNMENT);
		close.setAlignmentX(Component.LEFT_ALIGNMENT);
		add.addActionListener(this);
		close.addActionListener(this);
		buttonGroup.add(add);
		buttonGroup.add(close);

		window.add(title);
		window.add(category);
		window.add(cost);
        window.add(tail);
        window.add(buttonGroup);

		add(window);

		setTitle(name);
		pack();
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);

	}

    public abstract JPanel specialAdd();


    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
		switch(e.getActionCommand()) {
			case "Add": {
				addItem();
				break;
			}
			case "Close": {
				dispose();
				break;
			}
		}
        
    }
    public abstract void addItem();


	public JTextField gTitle() {
		return title;
	}

	public void sTitle(JTextField title) {
		this.title = title;
	}

	public JTextField gCategory() {
		return category;
	}

	public void sCategory(JTextField category) {
		this.category = category;
	}

	public JTextField gCost() {
		return cost;
	}

	public void sCost(JTextField cost) {
		this.cost = cost;
	}
	
}
