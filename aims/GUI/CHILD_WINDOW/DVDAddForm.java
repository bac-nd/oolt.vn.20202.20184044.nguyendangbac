package GUI.CHILD_WINDOW;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import GUI.Window;
import media.DigitalVideoDisc;
import order.Order;

public class DVDAddForm extends Chill{
    public Window gWindow() {
        return window;
    }

    public void sWindow(Window window) {
        this.window = window;
    }

    private JTextField director;
    private JTextField length;

    public JTextField gDirector() {
        return director;
    }

    public void sDirector(JTextField director) {
        this.director = director;
    }

    public JTextField gLength() {
        return length;
    }

    public void sLength(JTextField length) {
        this.length = length;
    }

    private Window window;

    public DVDAddForm(Window window, String name) {
        super(name);
        this.window = window;
        //TODO Auto-generated constructor stub
    }

    @Override
    public JPanel specialAdd() {
        // TODO Auto-generated method stub
        director = new JTextField(37);
        length = new JTextField(37);

        JPanel result = new JPanel();
        result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));
        JPanel dir = new JPanel();
        dir.setBorder(BorderFactory.createTitledBorder("Director:"));
        dir.add(director);

        JPanel len = new JPanel();
        len.setBorder(BorderFactory.createTitledBorder("Length:"));
        len.add(length);

        result.add(dir);
        result.add(len);
        return result;
    }

    @Override
    public void addItem() {
        // TODO Auto-generated method stub
        String title = this.gTitle().getText();
        String category = this.gCategory().getText();
        String cost = this.gCost().getText();
        String director = this.director.getText();
        String length = this.length.getText();
        if (title == "") {
            new Error();
            return;
        }
        if (category == "") {
            window.getOrder().addMedia(new DigitalVideoDisc(title));
            window.update();
            return;
        }
        if (cost == "") {
            window.getOrder().addMedia(new DigitalVideoDisc(title, category));
            window.update();
            return;
        }
        for (int i = 0; i < cost.length(); i++) {
            if (cost.charAt(i) < '0' || cost.charAt(i) > '9' || cost.charAt(i) != '.') {
                new Error();
                return;
            }
        }
        if (length == "") {
            window.getOrder().addMedia(
                new DigitalVideoDisc(title, category, Float.parseFloat(cost))
            );
            window.update();
            return;
        }
        for (int i = 0; i < length.length(); i++) {
            if (length.charAt(i) < '0' || length.charAt(i) > '9') {
                new Error();
                return;
            }
        }
        if (director == "") {
            window.getOrder().addMedia(
                new DigitalVideoDisc(
                    title, category, 
                    Float.parseFloat(cost), 
                    Integer.parseInt(length)
                )
            );
            window.update();
            return;
        }
        window.getOrder().addMedia(
                new DigitalVideoDisc(
                    title, category, 
                    Float.parseFloat(cost), 
                    Integer.parseInt(length),
                    director
                )
            );
            window.update();
            return;
    }

}
