
import javax.swing.JOptionPane;


public class showTwoNumber {
    public static void main(String [] args) {
        String strNumber1, strNumber2;
        String strNotification = "You are just enterd: ";
        strNumber1 = JOptionPane.showInputDialog(
            null, 
            "Please input the first number",
            "Input the first number",
            JOptionPane.INFORMATION_MESSAGE
        );
        strNotification += strNumber1 + "and";

        strNumber2 = JOptionPane.showInputDialog(
            null, 
            "Please input the second number",
            "Input the second number",
            JOptionPane.INFORMATION_MESSAGE
        );
        strNotification += strNumber2;

        JOptionPane.showMessageDialog(null, strNotification, "Show Two Number ", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
