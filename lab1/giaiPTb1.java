
import javax.swing.JOptionPane;

public class giaiPTb1 {
    public static void main(String [] args) {
        String strNumber, strNumbers [];
        Double a = 0.0, b = 0.0;
        do{
            strNumber = JOptionPane.showInputDialog(
                null, 
                "Input a b:",
                "ax+b=0",
                JOptionPane.INFORMATION_MESSAGE
            );
            strNumbers = strNumber.split(" ");
            if (strNumbers.length != 2) {
                a = 0.0;
            } else {
                a = Double.parseDouble(strNumbers[0]);
                b = Double.parseDouble(strNumbers[1]);
            }
            
        } while(a == 0);

        Double kq = -b/a;

        JOptionPane.showMessageDialog(null,a+"x+"+b+"=0\n=> x="+kq);
        System.exit(0);
    }
}
