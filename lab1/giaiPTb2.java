import javax.swing.JOptionPane;
public class giaiPTb2 {
    
    public static void main(String [] args) {
        String strNumber, strNumbers [];
        Double a = 0.0, b = 0.0, c = 0.0;
        // nhap he so phuong trinh 
        do{
            strNumber = JOptionPane.showInputDialog(
                null, 
                "Input a b c:",
                "ax^2+bx+c = 0",
                JOptionPane.INFORMATION_MESSAGE
            );
            strNumbers = strNumber.split(" ");
            if (strNumbers.length != 3) {
                a = 0.0;
            } else {
                a = Double.parseDouble(strNumbers[0]);
                b = Double.parseDouble(strNumbers[1]);
                c = Double.parseDouble(strNumbers[2]);
            }
            
        } while(a == 0);

        Double x1, x2;
        String kq = "";

        Double DELTA = b*b-4*a*c;
        if (DELTA == 0) {
            kq = "Nghiem kep: x="+(-b/2/a);
        } else if (DELTA < 0){
            kq = "Vo nghiem";
        } else if (DELTA > 0) {
            x1 = (-b+Math.sqrt(DELTA))/2/a;
            x2 = (-b-Math.sqrt(DELTA))/2/a;
            kq = "x1="+x1+"\nx2="+x2;
        }

        JOptionPane.showMessageDialog(null,a+"x^2+"+b+"x+"+c+"=0"+"\n"+kq);
        System.exit(0);
    }
}
