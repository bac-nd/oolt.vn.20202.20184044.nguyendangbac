import javax.swing.JOptionPane;
public class hePTb1_2an {
    
    public static void main(String [] args) {
        String strNumber1, strNumber2, strNumbers1 [], strNumbers2 [];
        Double a1 = 0.0, b1 = 0.0, c1 = 0.0, a2 = 0.0, b2 = 0.0, c2 = 0.0;
        // nhap he so phuong trinh thu nhat
        do{
            strNumber1 = JOptionPane.showInputDialog(
                null, 
                "Input a1 b1 c1:",
                "a1x+b1y=c1",
                JOptionPane.INFORMATION_MESSAGE
            );
            strNumbers1 = strNumber1.split(" ");
            if (strNumbers1.length != 3) {
                a1 = 0.0;
                b1 = 0.0;
                c1 = 0.0;
            } else {
                a1 = Double.parseDouble(strNumbers1[0]);
                b1 = Double.parseDouble(strNumbers1[1]);
                c1 = Double.parseDouble(strNumbers1[2]);
            }
            
        } while(a1 == 0 && b1 == 0 && c1 == 0);
        // nhap he so phuong trinh thu hai
        do{
            strNumber2 = JOptionPane.showInputDialog(
                null, 
                "Input a2 b2 c2:",
                "a2x+b2y=c2",
                JOptionPane.INFORMATION_MESSAGE
            );
            strNumbers2 = strNumber2.split(" ");
            if (strNumbers2.length != 3) {
                a2 = 0.0;
                b2 = 0.0;
                c2 = 0.0;
            } else {
                a2 = Double.parseDouble(strNumbers2[0]);
                b2 = Double.parseDouble(strNumbers2[1]);
                c2 = Double.parseDouble(strNumbers2[2]);
            }
            
        } while(a2 == 0 && b2 == 0 && c2 == 0);

        Double x, y;
        String kq;
        // giai theo dinh thuc
        Double D = a1*b2 - a2*b1;
        Double D1 = a1*c2 - a2*c1;
        Double D2 = c1*b2 - c2*b1;

        if (D == 0) {
            if (D1 == 0 && D2 == 0) {
                kq = "He vo so nghiem";
            } else {
                kq = "He vo nghiem";
            }
        } else {
            x = D2/D;
            y = D1/D;
            kq = "x="+x+"\ny="+y;
        }

        JOptionPane.showMessageDialog(null,a1+"x+"+b1+"y="+c1+"\n"+a2+"x+"+b2+"y="+c2+"\n"+kq);
        System.exit(0);
    }
}
