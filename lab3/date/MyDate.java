import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class MyDate{
    private int day;
    private int month;
    private int year;
    private String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};

    public MyDate() {
        LocalDate TODAY = LocalDate.now();
        this.setDate(TODAY);
    }
    
    public MyDate(int year, int month, int day) {
        if (!this.checkDate(year, month, day)) {
            System.out.println("ERROR");
        } else {
            this.year = year;
            this.month = month;
            this.year = year;
        }
    }
    
    public MyDate(String dateString) {
        this.accept(dateString);
    }

    public void setDate(LocalDate date) {
        this.day = date.getDayOfMonth();
        this.month = date.getMonthValue();
        this.year = date.getYear();
    }

    public boolean checkDate(int year, int month, int day) {
        String dateString = year + "-" + month + "-" + day;
        try {
            LocalDate date = LocalDate.parse(dateString);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    public int getYear() {
        return this.year;
    }
    public boolean setYear(int newYear) {
        if (this.checkDate(newYear, this.month, this.day)) {
            this.year = newYear;
            return true;
        }
        return false;
    }
    public int getMonth() {
        return this.month;
    }
    public boolean setMonth(int newMonth) {
        if (this.checkDate(this.year, newMonth, this.day)) {
            this.month = newMonth;
            return true;
        }
        return false;
    }
    public int getDay() {
        return this.day;
    }
    public boolean setDay(int newDay) {
        if (this.checkDate(this.year, this.month, newDay)) {
            this.day = newDay;
            return true;
        }
        return false;
    }

    public void accept(String dateString) {
        int date[] = this.readDateString(dateString);
        if (date[0] == -1) {
            System.out.println("ERROR!!!");
        } else {
            String day = date[0] + "";
            String month = date[1] + "";
            if (date[0] < 10) {
                day = "0" + day;
            }
            if (date[1] < 10) {
                month = "0" + month;
            }
            String formatted = date[2] + "-" + month + "-" + day;
            try {
                LocalDate newDate = LocalDate.parse(formatted);
                this.setDate(newDate);
            } catch(DateTimeParseException e) {
                System.out.println("ERROR");
            }
        }
    }
        
    public int[] readDateString(String dateString) {
        String tempString = dateString;
        int date[] = new int [3];
        String[] token = tempString.split("[ /-]");
        if (token.length != 3) {
            date[0] = -1;
            return date;
        }

        try {
            int tempNum = Integer.parseInt(token[0]);
            if (tempNum > 999) {
                date[0] = Integer.parseInt(token[2]);
                date[1] = Integer.parseInt(token[1]);
                date[2] = Integer.parseInt(token[0]);
            } else {
                date[0] = Integer.parseInt(token[0]);
                date[1] = Integer.parseInt(token[1]);
                date[2] = Integer.parseInt(token[2]);
            }
        } catch (NumberFormatException e) {
            date[1] = this.getIndexMonth(token[0]);
        // System.out.println(token[0].length());

            date[0] = Integer.parseInt(token[1]);
            date[2] = Integer.parseInt(token[2]);
        }

        return date;
    }

    public int getIndexMonth(String month) {
        for (int i = 0; i < 12; i++) {
            if (month.equals(months[i])) {
                return i+1;
            }
        }
        return -1;
    }
    
    public void print() {
        System.out.println(this.day + "-" + this.month + "-" + this.year);
    }
}