public class Order {
    private int qtyOrdered = 0;
    public static final int MAX_NUMBER_ORDER = 10;
    private DigitalVideoDisc itemsOder[] = new DigitalVideoDisc[MAX_NUMBER_ORDER];

    public int getQtyOrdered() {
        return this.qtyOrdered;
    }
    public void setQtyOrdered(int newQtyOrdered) {
        this.qtyOrdered = newQtyOrdered;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc newItem) {
        if (this.qtyOrdered == MAX_NUMBER_ORDER) {
            System.out.println("ERROR");
            return;
        }
        System.out.println(("SUCCESS"));
        this.itemsOder[qtyOrdered++] = newItem;
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc item) {
        if (this.qtyOrdered == 0) {
            System.out.println("Array is emty");
            return;
        }
        if (this.itemsOder[this.qtyOrdered-1] == item) {
            this.qtyOrdered--;
            System.out.println("Deleted");
            return;
        }
        int index = 0;
        for (; index < this.qtyOrdered-1; index++) {
            if (this.itemsOder[index] == item) {
                for (int i = index; i <= this.qtyOrdered; i++) {
                    this.itemsOder[i] = this.itemsOder[i+1];
                }
                this.qtyOrdered--;
                System.out.println("Deleted");
                return;
            }
        }
    }

    public float totalCost() {
        float total = 0.0f;
        for (int i = 0; i < this.qtyOrdered; i++) {
            total += this.itemsOder[i].getCost();
        }
        return total;
    }
}
