package GUI.BASIC_WINDOW;

import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import DATA.DVDList;
import GUI.Aims;
import GUI.BASIC_WINDOW.DEFAULT_TABLE.DefaultTable;
import media.DigitalVideoDisc;
import media.Media;
import order.Order;

public class DVDAddForm extends Basic{
    private JTable table;
    private DefaultTableModel model;

    public DVDAddForm(Aims aims, String title) {
        super(aims, title);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void addItem() {
        // TODO Auto-generated method stub
        Order o = this.getAims().getOrder();
        DVDList d = this.getAims().getDvdList();

        for (int i = 0; i < table.getRowCount(); i++) {
            boolean b = Boolean.valueOf(table.getValueAt(i, 6).toString());
            if (b) {
                if (o.addMedia(d.getList().get(i))) {
                    table.setValueAt(false, i, 6);
                    dispose();
                    getAims().getWindow().update();
                } else {
                    new Error("has exceeded the amount");
                    getAims().getWindow().update();
                    break;
                }
            }
        }
    }

    @Override
    public void setTable() {
        // TODO Auto-generated method stub
        JPanel result = new JPanel();
        model = new DefaultTable(6).init();
        table = new JTable();
        JScrollPane scroll = new JScrollPane(table);
        
        model.addColumn("ID");
        model.addColumn("Title");
        model.addColumn("Category");
        model.addColumn("Cost");
        model.addColumn("Length");
        model.addColumn("Director");
        model.addColumn("Select");

        table.setModel(model);

        table.getColumnModel().getColumn(0).setMaxWidth(30);
        table.getColumnModel().getColumn(6).setMaxWidth(50);

        result.add(scroll);
        List<Media> list = this.getAims().getDvdList().getList();
        for (int i = 0; i < list.size(); i++) {
            model.addRow(new Object[0]);
            model.setValueAt(list.get(i).getId(), i, 0);
            model.setValueAt(list.get(i).getTitle(), i, 1);
            model.setValueAt(list.get(i).getCategory(), i, 2);
            model.setValueAt(list.get(i).getCost(), i, 3);
            model.setValueAt(((DigitalVideoDisc)list.get(i)).getLength(), i, 4);
            model.setValueAt(((DigitalVideoDisc)list.get(i)).getDirector(), i, 5);
            model.setValueAt(false, i, 6);
        }
        getRoot().add(result);

    }

    @Override
    public void setHead() {
        // TODO Auto-generated method stub
        
    }

}
