package GUI.BASIC_WINDOW;

import javax.swing.*;

import GUI.Aims;

import java.awt.*;
import java.awt.event.*;

public abstract class Basic extends JFrame implements ActionListener{
	private JButton add;
	private JButton close;
	private JButton delItems;
	private JButton delCD;

	private JPanel root;

	private Aims aims;

    public Basic(Aims aims, String title) {
		this.aims = aims;
		setTitle(title);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBackground(Color.LIGHT_GRAY);

		root = new JPanel();
		root.setLayout(new BoxLayout(root, BoxLayout.Y_AXIS));
		root.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		add = new JButton("Add selected items");
		close = new JButton("Close");
		delItems = new JButton("Delete selected items");
		delCD = new JButton("Delete this CD");

		add.addActionListener(this);
		close.addActionListener(this);
		// delCD.addActionListener(this);
		delItems.addActionListener(this);

		setHead();
		setTable();
		setButtons();
		
		add(root);

		pack();
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
    }

    public Aims getAims() {
		return aims;
	}

	public void setAims(Aims aims) {
		this.aims = aims;
	}

	@Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
		switch(e.getActionCommand()) {
			case "Add selected items": {
				addItem();
				break;
			}
			case "Close": {
				dispose();
				break;
			}
			case "Delete selected items": {
				break;
			}
		}
        
    }

	public abstract void addItem();

	public abstract void setHead();;
	public abstract void setTable();

	public void setButtons() {
		JPanel buttonGroup = new JPanel();
		buttonGroup.setLayout(new BoxLayout(buttonGroup, BoxLayout.X_AXIS));

		buttonGroup.add(add);
		buttonGroup.add(close);
		
		root.add(buttonGroup);
	};

	public JPanel getRoot() {
		return root;
	}
}
