package GUI.BASIC_WINDOW;

import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import DATA.CDList;
import GUI.Aims;
import GUI.Window;
import GUI.BASIC_WINDOW.DEFAULT_TABLE.DefaultTable;
import media.CompactDisc;
import media.Media;
import order.Order;

public class CDAddForm extends Basic{
    private JTable table;
    private DefaultTableModel model;

    public CDAddForm(Aims aims, String title) {
        super(aims, title);
        //TODO Auto-generated constructor stub
    }

    @Override
    public void addItem() {
        // TODO Auto-generated method stub
        Order o = this.getAims().getOrder();
        CDList cd = this.getAims().getCDList();

        for (int i = 0; i < table.getRowCount(); i++) {
            boolean b = Boolean.valueOf(table.getValueAt(i, 5).toString());
            if (b) {
                if (o.addMedia(cd.getList().get(i))) {
                    table.setValueAt(false, i, 5);
                    dispose();
                    getAims().getWindow().update();
                } else {
                    new Error("has exceeded the amount");
                    getAims().getWindow().update();
                    break;
                }
            }
        }
    }

    public void setTable() {
        JPanel result = new JPanel();
        table = new JTable();
        model = new DefaultTable(5).init();
        JScrollPane scroll = new JScrollPane(table);
        
        model.addColumn("ID");
        model.addColumn("Title");
        model.addColumn("Category");
        model.addColumn("Cost");
        model.addColumn("Artist");
        model.addColumn("Select");

        table.setModel(model);

        table.getColumnModel().getColumn(0).setMaxWidth(30);
        table.getColumnModel().getColumn(5).setMaxWidth(50);

        result.add(scroll);

        List<Media> list = this.getAims().getCDList().getList();
        for (int i = 0; i < list.size(); i++) {
            model.addRow(new Object[0]);
            model.setValueAt(list.get(i).getId(), i, 0);
            model.setValueAt(list.get(i).getTitle(), i, 1);
            model.setValueAt(list.get(i).getCategory(), i, 2);
            model.setValueAt(list.get(i).getCost(), i, 3);
            model.setValueAt(((CompactDisc)list.get(i)).getArtist(), i, 4);
            model.setValueAt(false, i, 5);
        }

        getRoot().add(result);
    }

    @Override
    public void setHead() {
        // TODO Auto-generated method stub
        
    }

}
