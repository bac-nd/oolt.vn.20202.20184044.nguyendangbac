package GUI.BASIC_WINDOW.DEFAULT_TABLE;

import javax.swing.table.DefaultTableModel;

public class DefaultTable {
    private int []trueCol;

    public DefaultTable(int ...col) {
        trueCol = col;    
    }
    public DefaultTableModel init() {
        return new DefaultTableModel() {
			public Class<?> getColumnClass(int col) {
				for (int i = 0; i < trueCol.length; i++) {
                    if (col == trueCol[i]) return Boolean.class;
                }
                return String.class;
			}
			public boolean isCellEditable(int row, int col) {
				for (int i = 0; i < trueCol.length; i++) {
                    if (col == trueCol[i]) return true;
                }
                return false;
			}
		};
    }
}
