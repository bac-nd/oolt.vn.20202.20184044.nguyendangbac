package GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import order.Order;
import media.*;
import GUI.BASIC_WINDOW.*;
import GUI.BASIC_WINDOW.DEFAULT_TABLE.DefaultTable;

import java.awt.event.*;
import java.util.List;
import java.awt.*;

public class Window extends JFrame implements ActionListener {
	private String caption;
	private JButton addBook;
	private JButton addDVD;
	private JButton addCD;
	private JButton deleteTtem;
	private JButton close;
	private JTable table;
	private JLabel costTotal;
	private DefaultTableModel model;

	private Aims aims;
	
	public Window(Aims aims) {
		caption = "THE ITEMS LIST OF ORDER";
		addBook = new JButton("Book");
		addDVD = new JButton("DVD");
		addCD = new JButton("CD");
		deleteTtem = new JButton("Delete selected items");
		close = new JButton("Close");

		costTotal = new JLabel("Cost total: 0$");

		addBook.addActionListener(this);
		addDVD.addActionListener(this);
		addCD.addActionListener(this);
		deleteTtem.addActionListener(this);
		close.addActionListener(this);

		table = new JTable();


		this.aims = aims;
		init();

	}

	public void init() {

		JPanel window = new JPanel();
		window.setBackground(Color.LIGHT_GRAY);
		window.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		window.setLayout(new BoxLayout(window, BoxLayout.Y_AXIS));

		JPanel upWindow = new JPanel();
		upWindow.setBorder(BorderFactory.createTitledBorder(caption));
		upWindow.setBackground(Color.WHITE);
		JScrollPane list = new JScrollPane(table);
		model = new DefaultTable(2).init();
		model.addColumn("ID");
		model.addColumn("Info");
		model.addColumn("Select");
		update();
		table.setModel(model);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		table.getColumnModel().getColumn(2).setMaxWidth(50);
		// table.setModel(arg0);
		upWindow.add(list);

		JPanel centerWindow = new JPanel();
		centerWindow.add(costTotal);

		JPanel downWindow = new JPanel();
		downWindow.setLayout(new BoxLayout(downWindow, BoxLayout.X_AXIS));
		downWindow.setBorder(BorderFactory.createEmptyBorder(14, 14, 14, 14));
		downWindow.add(addBook);
		downWindow.add(addDVD);
		downWindow.add(addCD);
		downWindow.add(deleteTtem);
		downWindow.add(close);
		// downWindow.add(close);
		
		window.add(upWindow);
		window.add(centerWindow);
		window.add(downWindow);
		add(window);
		pack();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Aims Project");

		setResizable(false);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		switch(e.getActionCommand()) {
			case "Book": {
				new BookAddForm(this.aims, "Book List");
				break;
			}
			case "DVD": {
				new DVDAddForm(this.aims, "DVD List");
				break;
			}
			case "CD": {
				new CDAddForm(this.aims, "CD List");
			}
			case "Delete selected items": {
				deleteItems();
				break;
			}
			case "Close": {
				dispose();
				break;
			}
		}
		// System.out.println(e.getActionCommand());
	}

	public void update() {
		model.setRowCount(0);
		List<Media> list = aims.getOrder().getItemsOrdered();
		for (int i = 0; i < list.size(); i++) {
			model.addRow(new Object[0]);
			model.setValueAt(list.get(i).getId(), i, 0);
			model.setValueAt(list.get(i).outString(), i, 1);
			model.setValueAt(false, i, 2);
		}
		costTotal.setText("Cost total: "+Math.floor(aims.getOrder().totalCost()*100)/100+"$");
	}

	public void deleteItems() {
		Order o = this.aims.getOrder();

		for (int i = 0; i < table.getRowCount(); i++) {
            boolean b = Boolean.valueOf(table.getValueAt(i, 2).toString());
            if (b) {
				o.removeMedia(o.searchMedia((int)(table.getValueAt(i, 0))));
            }
        }
		update();
	}
}
