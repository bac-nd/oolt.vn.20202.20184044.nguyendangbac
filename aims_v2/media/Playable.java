package media;

import EXCEPTION.PlayerException;

public interface Playable {
	public void play() throws PlayerException;
}
