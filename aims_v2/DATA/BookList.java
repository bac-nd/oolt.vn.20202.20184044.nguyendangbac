package DATA;

import java.util.*;

import media.Book;
import media.Media;

public class BookList {
    private List<Media> list = new ArrayList<>();

    public BookList() {
        list.add(new Book("Nightmare", "Chemical periodic table", 100.0f, "Dmitri Ivanovich Mendeleev"));
        list.add(new Book("Fiction", "Doraemon", 20.05f, "Fujiko F. Fujio"));
    }

    public List<Media> getList() {
        return list;
    }

}
