package DATA;

import java.util.*;

import media.DigitalVideoDisc;
import media.Media;

public class DVDList {
    private List<Media> list = new ArrayList<>();

    public DVDList() {
        list.add(new DigitalVideoDisc("The Lion King", "Animation", 19.95f, 87, "Roger Allers"));
        list.add(new DigitalVideoDisc("Java world", "Codeventure", 69.96f, 90, "Mad Demon"));
        list.add(new DigitalVideoDisc("A beautiful hello World", "Science Fiction", 90.9f, 89, "Peter Scripter"));
    
    }

    public List<Media> getList() {
        return list;
    }
}
