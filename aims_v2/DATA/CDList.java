package DATA;

import java.util.*;

import media.CompactDisc;
import media.Media;
import media.Track;

public class CDList {
    private List<Media> list = new ArrayList<>();

    public CDList() {
        Track track1 = new Track("Hello", 12);
        Track track2 = new Track("Send my love", 10);
        Track track3 = new Track("Someone like you", 8);
        Track track4 = new Track("All i ask", 21);
        Track track5 = new Track("Skyfall", 15);
        list.add(new CompactDisc("Depression", "Classic", 245.55f, "Adele",track1, track2, track3, track4, track5));
    }

    public List<Media> getList() {
        return list;
    }

}
