package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();

    public Book(String title){
        super(title);
    }

    public Book(String title, String category){
        super(title, category);
    }

    public Book(String title, String category, List<String> authors){
        super(title, category);
        this.authors = authors;
        //TODO: check author condition
    }

    public Book(String title, String category, List<String> authors, float cost) {
        this(title, category, authors);
        this.cost = cost;
    }

    public void setAuthorName(List<String> authors) {
        this.authors = authors;
    }

    public String[] getAuthors() {
        String[] list = new String[authors.size()];
        for (int i = 0; i < authors.size(); i++) {
            list[i] = authors.get(i);
        }
        return list;
    }

    public void addAuthors(String authorsName) {
        if (!(authors.contains(authorsName))) {
            authors.add(authorsName);
        };
    }

    public void removeAuthor(String authorName) {
        if (!(authors.contains(authorName))) {
            authors.remove(authorName);
        };
    }

    public String printOut() {
        return (
            ".BOOK - " +
            this.getTitle() + " - " +
            this.getCategory() + " - " +
            String.join(", ", this.getAuthors()) + " - " +
            this.getCost()
        );
    }

}