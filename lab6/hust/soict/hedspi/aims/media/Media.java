package hust.soict.hedspi.aims.media;

public class Media {
    protected String title;
    protected String category;
    protected float cost;

    public Media(String title){
        this.title = title;
    }

    public Media(String title, String category){
        this(title);
        this.category = category;
    }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String newTitle) {
        this.title = newTitle;
    }
    public String getCategory() {
        return this.category;
    }
    public void setCategory(String newCategory) {
        this.category = newCategory;
    }
    public float getCost() {
        return this.cost;
    }
    public void setCost(float newCost) {
        this.cost = newCost;
    }
    public String printOut() {
        return "";
    }
}
