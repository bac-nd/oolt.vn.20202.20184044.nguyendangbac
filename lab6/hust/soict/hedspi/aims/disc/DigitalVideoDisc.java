package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Media;

public class DigitalVideoDisc extends Media {
    protected String director;
    protected int length;

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category);

        this.director = director;
        this.length = length;
        this.cost = cost;
    }
    
        public DigitalVideoDisc(String title) {
            super(title);
        }

    public Boolean search(String Title) {
        String title = this.title;
        String tokensKeyWord[] = Title.split("[ ]");

        for (int i = 0; i < tokensKeyWord.length; i++) {
            if (title.toLowerCase().contains(tokensKeyWord[i].toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public String getDirector() {
        return this.director;
    }
    public void setDirector(String newDirector) {
        this.director = newDirector;
    }

    public int getLength() {
        return this.length;
    }
    public void setLength(int newLength) {
        this.length = newLength;
    }

    public String printOut() {
        return (
            ".DVD - " +
            this.getTitle() + " - " +
            this.getCategory() + " - " +
            this.getDirector() + " - " +
            this.getLength() + " - " +
            this.getCost()
        );
    }
}
