package hust.soict.hedspi.aims;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.hedspi.aims.disc.*;
import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.order.*;

public class Aims {
    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1.Create new order");
        System.out.println("2.Add item to the order");
        System.out.println("3.Delete item by id");
        System.out.println("4.Display the items list of order");
        System.out.println("0.Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        int select = -1;
        Order order = null;
        do {
            showMenu();
            select = input.nextInt();
            switch(select) {
                case 1: {
                    if (order != null) {
                        System.out.println("already exist!!!");
                    } else {
                        order = new Order();
                        System.out.println("Created");
                    }
                    break;
                }
                case 2: {
                    int select2 = -1;
                    do{
                        System.out.println("1.Book");
                        System.out.println("2.DVD");
                        System.out.println("0.Back");
                        select2 = input.nextInt();
                        switch (select2) {
                            case 1: {
                                String title = "";
                                System.out.println("Enter title:");
                                title = input.nextLine();

                                Book b = new Book(title);

                                System.out.println("Enter authors ('.' to end):");
                                String name = "";
                                while (true) {
                                    name = input.nextLine();

                                    if (name == ".") {
                                        break;
                                    }
                                    b.addAuthors(name);
                                }

                                String category = "";
                                System.out.println("Enter category:");
                                category = input.nextLine();
                                b.setCategory(category);

                                float cost = 0f;
                                System.out.println("Enter cost:");
                                cost = input.nextFloat();
                                b.setCost(cost);

                                order.addMedia(b);
                            }
                            case 2: {
                                String title = "";
                                System.out.println("Enter title:");
                                title = input.nextLine();

                                DigitalVideoDisc d = new DigitalVideoDisc(title);

                                String category = "";
                                System.out.println("Enter category:");
                                category = input.nextLine();
                                d.setCategory(category);

                                String director = "";
                                System.out.println("Enter director:");
                                director = input.nextLine();
                                d.setDirector(director);

                                int length = 0;
                                System.out.println("Enter length:");
                                length = input.nextInt();
                                d.setLength(length);

                                float cost = 0f;
                                System.out.println("Enter cost:");
                                cost = input.nextInt();
                                d.setCost(cost);

                                order.addMedia(d);
                            }
                        }
                    } while(select2 != 0);

                    break;
                }
                case 3: {
                    int id = -1;
                    System.out.println("Enter ID:");
                    id = input.nextInt();
                    order.removeMedia(id);
                    break;
                }
                case 4: {
                    Order.printOrders();
                }
            }
        } while(select != 0);

        
        // Order anOder = new Order();

        // DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        // dvd1.setCategory("Animation");
        // dvd1.setCost(19.95f);
        // dvd1.setDirector("Roger Allers");
        // dvd1.setLength(87);
        // anOder.addMedia(dvd1);

        // DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wors");
        // dvd2.setCategory("Science Fiction");
        // dvd2.setCost(24.95f);
        // dvd2.setDirector("George Lucas");
        // dvd2.setLength(124);
        // anOder.addMedia(dvd2);

        // DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        // dvd3.setCategory("Animation");
        // dvd3.setCost(18.99f);
        // dvd3.setDirector("John Musker");
        // dvd3.setLength(90);
        // anOder.addMedia(dvd3);

        // List<String> authors = new ArrayList<String>();
        // authors.add("Nguyen Van A");
        // authors.add("Nguyen Van B");
        // authors.add("Nguyen Van C");
        // Book book1 = new Book("ABC", "Education", authors, 12);
        // anOder.addMedia(book1);
        // // anOder.removeDigitalVideoDisc(dvd3);

        // System.out.print("Total Cost is: ");
        // System.out.println(Order.totalCost());

        // Order.printOrders();
    }
}
