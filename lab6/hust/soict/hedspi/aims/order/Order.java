package hust.soict.hedspi.aims.order;
import java.util.ArrayList;

import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.utils.*;

public class Order {
    private static ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    private static MyDate dateOrdered = new MyDate();

    public static final int MAX_NUMBER_ORDER = 10;
    public static final int MAX_LIMITTED_ORDER = 5;
    private static int nbOrders = 0;

    public Order() {
        if (nbOrders >= MAX_LIMITTED_ORDER) {
            return;
        }
    }

    public static void printOrders() {
        getALuckyItem();
        System.out.println("************************Order"+
                            "************************");
        System.out.print("Date: ");
        System.out.println(dateOrdered.print()+"\nOrdered Items:");
        for (int i = 0; i < itemsOrdered.size(); i++) {
            System.out.println(
                (i+1) + itemsOrdered.get(i).printOut()
            );
        }
        System.out.println("Total cost: "+totalCost());
        System.out.println("*****************************************************");
    }

    public void addMedia(Media item1, Media item2) {
        this.addMedia(item1);
        this.addMedia(item2);
    }

    public void addMedia(Media [] itemList) {
        for (int i = 0; i < itemList.length; i++) {
            this.addMedia(itemList[i]);
        }
    }

    public void addMedia(Media newItem) {
        if (MAX_NUMBER_ORDER > itemsOrdered.size()) {
            itemsOrdered.add(newItem);
        }
    }

    public void removeMedia(Media item) {
        itemsOrdered.remove(item);
    }

    public void removeMedia(int id) {
        itemsOrdered.remove(id);
    }

    public static float totalCost() {
        float total = 0.0f;
        for (int i = 0; i < itemsOrdered.size(); i++) {
            total += itemsOrdered.get(i).getCost();
        }
        return total;
    }

    public static void getALuckyItem() {
        int randomNumber = (int)(Math.random()*(itemsOrdered.size()-1));
        itemsOrdered.get(randomNumber).setCost(0);
    }
}
