package hust.soict.hedspi.garbage.OtherProjects;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GarbageCreator {
    public static void main(String[] args) {
        StringBuffer s = new StringBuffer();
        try {
            File f = new File("hust/soict/hedspi/garbage/OtherProjects/garbage.txt");
            Scanner text = new Scanner(f);
            while (text.hasNextLine()) {
                s.append(text.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        System.out.println(s);
    }
}
