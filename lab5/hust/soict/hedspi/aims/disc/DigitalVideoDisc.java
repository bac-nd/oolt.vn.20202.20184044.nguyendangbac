package hust.soict.hedspi.aims.disc;

public class DigitalVideoDisc {
    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        this.title = title;
        this.category = category;
        this.director = director;
        this.length = length;
        this.cost = cost;
    }

    public Boolean search(String Title) {
        String title = this.title;
        String tokensKeyWord[] = Title.split("[ ]");

        for (int i = 0; i < tokensKeyWord.length; i++) {
            if (title.toLowerCase().contains(tokensKeyWord[i].toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public DigitalVideoDisc(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String newTitle) {
        this.title = newTitle;
    }
    public String getCategory() {
        return this.category;
    }
    public void setCategory(String newCategory) {
        this.category = newCategory;
    }
    public String getDirector() {
        return this.director;
    }
    public void setDirector(String newDirector) {
        this.director = newDirector;
    }
    public int getLength() {
        return this.length;
    }
    public void setLength(int newLength) {
        this.length= newLength;
    }
    public float getCost() {
        return this.cost;
    }
    public void setCost(float newCost) {
        this.cost = newCost;
    }

}
