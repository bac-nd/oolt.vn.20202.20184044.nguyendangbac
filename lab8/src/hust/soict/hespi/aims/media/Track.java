
public class Track implements Playable, Comparable {

	private String title;
	private int length;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Track() {
	}

	public Track(int length, String title) {
		this.length = length;
		this.title = title;
	}

	@Override
	public void play() {
		System.out.println("Playing Track: " + this.getTitle());
		System.out.println("Track's length: " + this.getLength());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + length;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;
		if (length != other.length)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Object o) {
		if (o instanceof Track) {
			return this.title.compareTo(((DigitalVideoDisc) o).title);
		}
		return -5;
	}
}
