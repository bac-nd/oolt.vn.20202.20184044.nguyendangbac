
public class MemoryDaemon implements Runnable {
	public long memory = 0;

	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;

		while (true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != memory) {
				System.out.println("\t Memory used = " + used);
				memory = used;
			}
		}
	}
}
