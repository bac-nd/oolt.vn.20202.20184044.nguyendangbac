
import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{

	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();

	public CompactDisc(String title) {
		super(title);
	}

	public CompactDisc(String category, String title) {
		super(category, title);
	}

	public CompactDisc(String category, String title, float cost) {
		super(category, title, cost);
	}

	public CompactDisc(String artist, String category, String title, float cost, Track... tracks) {
		super(category, title, cost);
		this.artist = artist;
		for (Track track : tracks) {
			this.addTrack(track);
		}
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public ArrayList<Track> getTracks() {
		return tracks;
	}

	public void setTracks(ArrayList<Track> tracks) {
		this.tracks = tracks;
	}

	public void addTrack(Track track) {
		tracks.add(track);
	}

	public void addTrack(Track... track) {
		for (Track item : tracks) {
			addTrack(item);
		}
	}

	public boolean removeTrack(Track item) {
		if (tracks.contains(item)) {
			tracks.remove(item);
			return true;
		} else {
			return false;
		}
	}

	public void CDInfo() {
		System.out.print("CD - [" + this.title + "]");
		System.out.print(" - [" + this.category + "]");
		System.out.print(": [" + this.cost + "]");
	}

	public void play() {
		System.out.println("Artist: " + this.artist);
		for (Track track : tracks) {
			track.play();
		}
	}

	public int getTrackNumbers() {
		return tracks.size();
	}
}