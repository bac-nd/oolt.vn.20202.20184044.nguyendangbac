
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Book extends Media {
	private String content = "This is the content of this book which is very interesting !";
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new HashMap<String, Integer>();
	private List<String> authors = new ArrayList<String>();

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public Book() {
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}

	public Book(String title) {
		super(title);
	}

	public Book(String title, String category) {
		super(title, category);
	}

	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}

	public Book(String title, String category, float cost, String... authors) {
		super(title, category, cost);
		for (String author : authors) {
			addAuthor(author);
		}
	}

	public void addAuthor(String authorName) {
		if (!(authors.contains(authorName))) {
			authors.add(authorName);
		}
	}

	public void removeAuthor(String authorName) {
		if (authors.contains(authorName)) {
			authors.remove(authorName);
		}
	}

	public void bookInfo() {
		System.out.print("Book - [" + title + "]");
		System.out.print(" - [" + category + "]");
		System.out.print(" - [");
		for (String author : authors) {
			System.out.print(author + " - ");
		}
		System.out.print("]");
		System.out.print(": [" + cost + "]");
	}

	public void processContent() {
		String content = this.content;
		String token[] = content.split(" ");

		for (int i = 0; i < token.length; i++) {
			contentTokens.add(token[i]);
		}

		Collections.sort(contentTokens);
	
		for (String s : contentTokens) {
			if (wordFrequency.containsKey(s)) {				
				int frequency = wordFrequency.get(s);
				wordFrequency.put(s, frequency + 1);
			} else {
				System.out.println(s);
			}
		}
	}

	@Override
	public String toString() {
		System.out.println("Title: " + this.getTitle());
		System.out.println("Cost: " + this.getCost());

		for (Object key : wordFrequency.keySet()) {
			System.out.println(key + ":" + wordFrequency.get(key));
		}
		
		return wordFrequency.toString();			
	}
}
