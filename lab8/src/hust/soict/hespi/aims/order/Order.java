package hust.soict.hedspi.aims.order;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.utils.MyDate;

public class Order {
	private List<Media> itemsOrdered = new ArrayList<Media>();
	private final LocalDate today = LocalDate.now();
	private MyDate dateOrdered;

	public Order() {

	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public void addMedia(Media item) {
		itemsOrdered.add(item);
	}

	public void addMedia(Media... items) {
		for (Media item : items) {
			addMedia(item);
		}
	}

	public boolean removeMedia(Media item) {
		if (itemsOrdered.contains(item)) {
			itemsOrdered.remove(item);
			return true;
		} else {
			return false;
		}
	}

	public boolean removeMedia(int id) {
		if (id < 1 || id > itemsOrdered.size()) {
			return false;
		} else {
			itemsOrdered.remove(id - 1);
			return true;
		}
	}

	public Media search(String title) {
		String token[] = title.toLowerCase().split(" ");
		int count = 0;

		for (Media item : itemsOrdered) {
			count = 0;
			for (int i = 0; i < token.length; i++) {
				if (item.getTitle().toLowerCase().contains(token[i])) {
					count++;
				}
			}
			if (count == token.length) {
				return item;
			}
		}
		return null;
	}

	public Media search(String title, String itemType) {
		String token[] = title.toLowerCase().split(" ");
		int count = 0;

		for (Media item : itemsOrdered) {
			count = 0;
			for (int i = 0; i < token.length; i++) {
				if (item.getTitle().toLowerCase().contains(token[i])) {
					count++;
				}
			}

			if (count == token.length && itemType == item.getType()) {
				return item;
			}
		}
		return null;
	}

	public void printOrderedItems() {
		System.out.println("\n***********************Order************************");
		System.out.print("Date: " + today);
		System.out.println("\nOrder Items:");

		int i = 1;
		for (Media item : itemsOrdered) {
			System.out.print((i++) + ".");
			if (item instanceof DigitalVideoDisc) {
				((DigitalVideoDisc) item).DVDInfo();
			} else if (item instanceof Book) {
				((Book) item).bookInfo();
			} else if (item instanceof CompactDisc) {
				((CompactDisc) item).CDInfo();
			}
			System.out.println();
		}
		System.out.println("\nTotal cost: " + totalCost() + "$");
		System.out.println("*******************************************************");
	}

	public float totalCost() {
		float total = 0;
		Media mediaItem;
		java.util.Iterator iter = itemsOrdered.iterator();
		while (iter.hasNext()) {
			mediaItem = (Media) (iter.next());
			total += mediaItem.getCost();
		}
		return total;
	}
}
