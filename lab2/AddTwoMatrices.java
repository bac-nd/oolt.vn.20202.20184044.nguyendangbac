import java.util.Scanner;

public class AddTwoMatrices {

    public static Scanner input = new Scanner(System.in);
    public static Float[][] A = new Float[10][10];
    public static Float[][] B = new Float[10][10];
    public static Float[][] C = new Float[10][10];
    public static int n, m;
    
    public static void inputMatric(Float M[][]) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                M[i][j] = input.nextFloat();
            }
        }
    }

    public static void addMatrices() {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
    }

    public static void printMatric(Float M[][]) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.format("%.2f\t", M[i][j]);
            }
            System.out.println();
        }
    }
    public static void main(String [] args) {
        System.out.print("Enter m: ");
        m = input.nextInt();
        System.out.print("Enter n: ");
        n = input.nextInt();

        System.out.println("Enter A matric:");
        inputMatric(A);
        System.out.println("Enter B matric:");
        inputMatric(B);

        addMatrices();

        System.out.println("Answer:");
        printMatric(C);
    }
}