import java.util.Scanner;

public class TinhNgay {
    public static void main(String [] args) {
        int months[] = {31,28,31,30,31,30,31,31,30,31,30,31};
        Scanner input = new Scanner(System.in);

        System.out.print("Enter year: ");
        int year = input.nextInt();
        
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
            months[1] = 29;
        }

        System.out.print("Enter month: ");
        int month = input.nextInt();

        System.out.println("Thang "+month+" nam "+year+" co "+months[month-1]+" ngay.");
    }
}
