import java.util.Scanner;

public class VeTamGiac {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);
        int h;
        do{
            System.out.println("Nhap chieu cao tam giac");
            h = input.nextInt();
        } while (h <= 0);

        int i, j, t;
        for (i = 0; i < h; i++) {

            for (j = 0; j < h-i; j++) {
                System.out.print(" ");
            }

            for (t = 0; t < 2*i-1; t++) {
                System.out.print("*");
            }
            System.out.println();

        }
    }
}
