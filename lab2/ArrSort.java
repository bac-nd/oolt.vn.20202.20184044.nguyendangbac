import java.util.Scanner;
import java.util.Arrays;

public class ArrSort {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Nhap so phan tu: ");
        int total = input.nextInt();
        
        System.out.println("Nhap cac phan tu:");
        int arr[] = new int [total];
        for (int i = 0; i < total; i++) {
            arr[i] = input.nextInt();
        }
        Arrays.sort(arr);
        System.out.print("Mang sau ki sap xep:\n"+ Arrays.toString((arr)));
    }
}