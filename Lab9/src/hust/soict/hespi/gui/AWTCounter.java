import java.awt.*;
import java.awt.event.*;

public class AWTCounter extends Frame implements ActionListener {
    private Label lblCount;
    private TextField tfCount;
    private Button btnCount;
    private int count;

    public AWTCounter() {
        setLayout(new FlowLayout());
        lblCount = new Label("Counter");
        add(lblCount);
        tfCount = new TextField(count + "", 10);
        tfCount.setEditable(false);
        add(tfCount);
        btnCount = new Button("Count");
        add(btnCount);
        btnCount.addActionListener(this);
        Button test = new Button("Subtract");
        add(test);
        test.addActionListener(this);
        setTitle("AWT Counter");
        setSize(150, 200);
        setLocationRelativeTo(null);
        setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        // ++count;
        // tfCount.setText(count + "");
        // System.out.println(e.getActionCommand());
        switch(e.getActionCommand()) {
            case "Count": {
                ++count;
                break;
            }
            case "Subtract": {
                --count;
                break;
            }
        }
        tfCount.setText(count + "");
    }

    public static void main(String[] args) {
        new AWTCounter();
    }
    
}
