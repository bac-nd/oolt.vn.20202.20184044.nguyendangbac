import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SwingCounter extends JFrame implements ActionListener{
    private JTextField tfCount;
    private JButton btnCount;
    private JButton btnSub;
    private int count = 0;

    public static void main(String[] args) {
        new SwingCounter();
    }
    public SwingCounter() {
        Container cp = getContentPane();
        cp.setLayout(new FlowLayout());
        tfCount = new JTextField("0", 10);
        tfCount.setEditable(false);
        btnCount = new JButton("count");
        btnSub = new JButton("subtract");
        btnCount.addActionListener(this);
        btnSub.addActionListener(this);
        cp.add(new JLabel("Counter"));
        cp.add(tfCount);
        cp.add(btnCount);
        cp.add(btnSub);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Swing Counter");
        setSize(200, 100);
        setLocationRelativeTo(null);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        // TODO Auto-generated method stub
        switch(arg0.getActionCommand()) {
            case "count": {
                ++count;
                break;
            }
            case "subtract": {
                --count;
                break;
            }
        }
        tfCount.setText(count+"");
    }

}
