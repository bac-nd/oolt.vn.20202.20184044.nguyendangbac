public class Order {
    private static int qtyOrdered = 0;
    public static final int MAX_NUMBER_ORDER = 10;
    private static DigitalVideoDisc itemsOder[] = new DigitalVideoDisc[MAX_NUMBER_ORDER];
    private static MyDate dateOrdered = new MyDate();
    public static final int MAX_LIMITTED_ORDER = 5;
    private static int nbOrders = 0;

    public Order() {
        if (MAX_LIMITTED_ORDER <= nbOrders) {
            return;
        }
        nbOrders++;
    }

    public static void printOrders() {
        System.out.println("************************Order"+
                            "************************");
        System.out.print("Date: ");
        System.out.println(dateOrdered.print()+"\nOrdered Items:");
        for (int i = 0; i < qtyOrdered; i++) {
            System.out.println(
                (i+1)+".DVD - "+
                itemsOder[i].getTitle()+" - "+
                itemsOder[i].getCategory()+" - "+
                itemsOder[i].getDirector()+" - "+
                itemsOder[i].getLength()+" : "+
                itemsOder[i].getCost()
            );
        }
        // System.out.println(nbOrders);
        System.out.println("Total cost: "+totalCost());
        System.out.println("*****************************************************");
    }



    public int getQtyOrdered() {
        return qtyOrdered;
    }
    public void setQtyOrdered(int newQtyOrdered) {
        qtyOrdered = newQtyOrdered;
    }

    public boolean addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
        if (qtyOrdered + 2 > MAX_NUMBER_ORDER) {
            return false;
        }
        this.addDigitalVideoDisc(dvd1);
        this.addDigitalVideoDisc(dvd2);
        return true;
    }

    public boolean addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
        if (dvdList.length + qtyOrdered > MAX_NUMBER_ORDER) {
            System.out.println("ERROR");
            return false;
        }
        for (int i = 0; i < dvdList.length; i++) {
            this.addDigitalVideoDisc(dvdList[i]);
        }
        return true;
    }

    public boolean addDigitalVideoDisc(DigitalVideoDisc newItem) {
        if (qtyOrdered == MAX_NUMBER_ORDER) {
            System.out.println("ERROR");
            return false;
        }
        System.out.println(("SUCCESS"));
        itemsOder[qtyOrdered++] = newItem;
        return true;
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc item) {
        if (qtyOrdered == 0) {
            System.out.println("Array is emty");
            return;
        }
        if (itemsOder[qtyOrdered-1] == item) {
            qtyOrdered--;
            System.out.println("Deleted");
            return;
        }
        int index = 0;
        for (; index < qtyOrdered-1; index++) {
            if (itemsOder[index] == item) {
                for (int i = index; i <= qtyOrdered; i++) {
                    itemsOder[i] = itemsOder[i+1];
                }
                qtyOrdered--;
                System.out.println("Deleted");
                return;
            }
        }
    }

    public static float totalCost() {
        float total = 0.0f;
        for (int i = 0; i < qtyOrdered; i++) {
            total += itemsOder[i].getCost();
        }
        return total;
    }
}
