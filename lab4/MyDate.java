import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class MyDate{
    private int day;
    private int month;
    private int year;
    private static String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private static String[] MONTHS = {
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    };
    private static String[] DAYS = {
        "First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eighth","Ninth",
        "Tenth","Eleventh","Twelfth","Thirteenth","Fourteenth","Fifteenth","Sixteenth","Seventeenth","Eighteenth","Nineteenth",
        "Twentieth","Twenty-first","Twenty-second","Twenty-third","Twenty-fourth","Twenty-fifth","Twenty-sixth","Twenty-seventh","Twenty-eight","Twenty-ninth",
        "Thirtisth","Thirty-first"
    };
    
    public MyDate() {
        LocalDate TODAY = LocalDate.now();
        this.setDate(TODAY);
    }

    public MyDate(int year, int month, int day) {
        if (!this.checkDate(year, month, day)) {
            System.out.println("ERROR");
        } else {
            this.year = year;
            this.month = month;
            this.year = year;
        }
    }
    
    public MyDate(String dateString) {
        this.accept(dateString);
    }

    public MyDate(String day, String month, String year) {
        this.day = this.getIndex(day, DAYS) + 1;
        String [] Year1 = {"one","two","three","four","five","six","seven","eight","nine","ten",
				"eleven","twelve","thirtheen","fourteen","fifty","sixteen","seventeen","eighteen","nineteen"
		};
		
		String [] Year2 = {"twenty","thirty","fourty","fifty","sixty","seventy","eighty","ninety"};
		
		if (!year.contains(" ")) {
			for (int i = 0; i < 19; i++)
				if (Year1[i].toLowerCase().contains(year)) {
					this.year = i+1+ 2000;
					break;
				}
			for (int j = 0 ; j < 8 ; j++) {
				if (Year2[j].toLowerCase().contains(year)) {
					this.year = (j+2)*10 + 2000;
					break;
				}
            }
		} else {
			String[] parts = year.split("[ ]");
			for (int i = 0; i < 8; i++) {
				if (Year2[i].toLowerCase().contains(parts[0])) {
					for (int j = 0; j < 19; j++) {
						if (Year1[j].toLowerCase().contains(parts[1])) {
							this.year = (i+2)*10 + j+1 + 2000;
							break;
						}
                    }
                }
            }
		}
    }

    public void setDate(LocalDate date) {
        this.day = date.getDayOfMonth();
        this.month = date.getMonthValue();
        this.year = date.getYear();
    }

    public boolean checkDate(int year, int month, int day) {
        String dateString = year + "-" + month + "-" + day;
        try {
            LocalDate date = LocalDate.parse(dateString);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    public int getYear() {
        return this.year;
    }
    public boolean setYear(int newYear) {
        if (this.checkDate(newYear, this.month, this.day)) {
            this.year = newYear;
            return true;
        }
        return false;
    }
    public int getMonth() {
        return this.month;
    }
    public boolean setMonth(int newMonth) {
        if (this.checkDate(this.year, newMonth, this.day)) {
            this.month = newMonth;
            return true;
        }
        return false;
    }
    public int getDay() {
        return this.day;
    }
    public boolean setDay(int newDay) {
        if (this.checkDate(this.year, this.month, newDay)) {
            this.day = newDay;
            return true;
        }
        return false;
    }

    public void accept(String dateString) {
        int date[] = this.readDateString(dateString);
        if (date[0] == -1) {
            System.out.println("ERROR!!!");
        } else {
            String day = date[0] + "";
            String month = date[1] + "";
            if (date[0] < 10) {
                day = "0" + day;
            }
            if (date[1] < 10) {
                month = "0" + month;
            }
            String formatted = date[2] + "-" + month + "-" + day;
            try {
                LocalDate newDate = LocalDate.parse(formatted);
                this.setDate(newDate);
            } catch(DateTimeParseException e) {
                System.out.println("ERROR");
            }
        }
    }
        
    public int[] readDateString(String dateString) {
        String tempString = dateString;
        int date[] = new int [3];
        String[] token = tempString.split("[ /-]");
        if (token.length != 3) {
            date[0] = -1;
            return date;
        }

        try {
            int tempNum = Integer.parseInt(token[0]);
            if (tempNum > 999) {
                date[0] = Integer.parseInt(token[2]);
                date[1] = Integer.parseInt(token[1]);
                date[2] = Integer.parseInt(token[0]);
            } else {
                date[0] = Integer.parseInt(token[0]);
                date[1] = Integer.parseInt(token[1]);
                date[2] = Integer.parseInt(token[2]);
            }
        } catch (NumberFormatException e) {
            date[1] = this.getIndex(token[0], months);
        // System.out.println(token[0].length());

            date[0] = Integer.parseInt(token[1]);
            date[2] = Integer.parseInt(token[2]);
        }

        return date;
    }

    public int getIndex(String str, String[] arr) {
        for (int i = 0; i < 12; i++) {
            if (str.equals(arr[i])) {
                return i+1;
            }
        }
        return -1;
    }
    
    public String print() {
        return (this.day + "-" + this.month + "-" + this.year);
    }
    public String print(String formatter) {
        String day, month, year;
        year = "" + this.year;
        month = "" + this.month;
        day = "" + this.day;

        if (this.day < 10) {
            day = "0" + this.day;
        }
        if (this.month < 10) {
            month = "0" + this.month;
        }

        if (formatter == "yyyy-MM-dd") {
            return (year + "-" + month + "-" + day);
        }
        if (formatter == "d/M/yyyy") {
            return (this.day + "/" + this.month + "/" + this.year);
        }
        if (formatter == "dd-MMM-yyyy") {
            return (day + "-" + this.months[this.month-1] + "-" + year);
        }
        if (formatter == "d MMM yyyy") {
            return (this.day + " " + this.months[this.month-1] + year);
        }
        if (formatter == "mm-dd-yyyy") {
            return (month + "-" + day + "-" + year);
        }
        return formatter;
    }
}