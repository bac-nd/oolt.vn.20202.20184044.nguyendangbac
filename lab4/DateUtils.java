public class DateUtils {
    
    public static int compareTwoDates(MyDate date1, MyDate date2) {
        if (date1.getYear() == date2.getYear()) {
            if (date1.getMonth() == date2.getMonth()) {
                if (date1.getDay() == date2.getDay()) {
                    return 0;
                }
                if (date1.getDay() > date2.getDay()) {
                    return -1;
                }
                return 1;
            }
            if (date1.getMonth() > date2.getMonth()) {
                return -1;
            }
            return 1;
        }
        if (date1.getYear() > date2.getYear()) {
            return -1;
        }
        return 1;
    }

    public static void sort(MyDate ...dates) {
        for (int i = 0; i < dates.length - 1; i++) {
            for (int j = i+1; j < dates.length; j++) {
                if (compareTwoDates(dates[i], dates[j]) == -1) {
                    MyDate temp = dates[i];
                    dates[i] = dates[j];
                    dates[j] = temp;
                }
            }
        }
    }
}
