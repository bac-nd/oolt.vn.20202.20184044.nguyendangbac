public class DateTest {
    
    public static void main(String [] args) {
        MyDate date = new MyDate("2000/02/01"); // yyyy/MM/dd
        MyDate date1 = new MyDate("13/12/2000"); // dd/MM/yyyy
        MyDate date2 = new MyDate("02 12 2000"); // dd MM yyyy
        MyDate date3 = new MyDate("10-12-2000"); // dd-MM-yyyy
        MyDate date4 = new MyDate("Feb 29 2020"); // 
        MyDate dates[] = {date, date1, date2, date3, date4};
        DateUtils.sort(dates);
        for (MyDate temp : dates) {
            System.out.println(temp.print("yyyy-MM-dd"));
        }
    }
}
